//JavaScript Synchronous vs Asynchronous

	//JS is by default synchronous, meaning that one statement is executed one at a time.

/*
	console.log("Hello World");

	cosnole.log("Hello from cosnole!");

	console.log("I'll run if no error occurs!")

*/

	//When certain statements take a lot of time to process, this slows down our code

	//An example of this are when loops are used in large amount of information or when fetching data from databases

/*
	for(let i=0; i<=1500; i++){

		console.log(i);
	}

	console.log("Hello Again!"); //only after the loop ends will this statement execute

*/

	//We might not notice it due to the fast processing power of devices

	//Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

/*
	API stands for application Programming Interface

	-is a particular set of codes that allows software programs to communicate with each other

	An API

	Fetch data from one application to another

*/

//Fetch api

	//FETCH
	//The fetch() method starts the process of fetcing a resource from a server
	//the fetch() method returns a PROMISE that is resolved to a Response object

	//The fetch api allows yout to asynchronousely request for a resource (data)

	//A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
	
	//A promise is in one of these three states:

	//Pending:
		//initial state, neither fulfilled nor rejected
	//Fulfilled:
		//operation was successfully completed
	//Rejected:
		//operation failed
	
	//Syntax
		//fetch("URL")

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*
	By using 'then' mehod we can now check for the status of the promise "fetch" method will return a PROMISE that resolves to be a response object ".then" method capture RESPONSE object and returns another promise which will eventually be "resolved" or "rejected"

	Syntax:
	fetch("URL")
	.then((response)=>{})

*/

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response=> console.log(response.status))

/*
	fetch function returns a promise. ".then" functions allows for processing that promise in order to extract data from it
	It also waits for the promise to be fulfilled before moving forward


*/

fetch("https://jsonplaceholder.typicode.com/posts")
//use the "json" method from the response object to convert the data retrieved into JSON format to be used in our applicaton
.then((response)=>response.json())
//print the converred JSON alue from our "fetch request"
//using multiple "then" methods creates a "promise chain"
.then((data)=>{
	console.log(data)
	console.log("This will run after the promise has been fulfilled")
})

console.log("This will run first")

/*
		The "async" and "await" keywords is another approach that can be used to achieve asynchronous codes
		-used in functions to indicate which portions of codes should be waited
		-creates and asynchronous function
*/

async function fetchData (){
	//waits for the fetch method to complete then stores the value in the result variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")

	//result returned by fetch is returned as a promise
	console.log(result);
	//returend response is a object
	console.log(typeof result)
	//we cannot access the content of the resposne by directly accessing its body property
	console.log(result.body);
	let json = await result.json();
	console.log(json);
}

fetchData();

//Get a specific post
//retrieves a specific post ff the REST API (retrieve, /posts/:id , GET)

fetch("https:jsonplaceholder.typicode.com/posts/1")
.then((response)=>response.json())
.then((data)=>console.log(data));

//create post
//create a new post following the REST API (create, /posts/:id, POST)

fetch("https://jsonplaceholder.typicode.com/posts",{
	method: "POST",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		title: "New post",
		body: "Hello World",
		userId: 1
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))

//updating a post
//update a specific post following the REST API (update, /posts/:id, PUT)

/*fetch("https://jsonplaceholder.typicode.com/posts",{
	method: "PUT",
	headers:{
		"Content-Type":"application/json"
	},
	body: JSON.stringify({
		id:1,
		title: "updated post",
		body:"hello again"
		userId: 1
	})
})
.then((response)=>response.json())
.then((data)=>console.log(data))

*/

//delete a post
//deleting a specific post following the REST API (delete, /posts/:id, DELETE)

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "DELETE"
})

//MINI ACTIVITY
//http://jsonplaceholder.typicode.com/todos GET method

fetch("https:jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())
.then((data)=>console.log(data));

//MA 2
//

fetch("https:jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())
.then((data)=>{
	let list = data.map((todo=>{
		return todo.title;
	}))
	console.log(list)
});
